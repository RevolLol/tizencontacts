function fsManager(){
	var that = this,
		//maybe new tizen.filesystem.file?
		currentDir = "",
		rootDir = "",
		copyQueue = [],
		watchStorageChangeId = tizen.filesystem.addStorageStateChangeListener(function(){
			
		});
	
	document.addEventListener("dirChange", function(oEvent){
		that.changeDir(oEvent.detail.dir, function(aEntries){
			document.dispatchEvent(new CustomEvent('fsChange', { 'detail': {data: aEntries, dir:currentDir} }));
		});
	});
	
	document.addEventListener("fileAction", function(oEvent){
		if(typeof oEvent.detail.action === "string")
		{
			switch(oEvent.detail.action.toLowerCase())
			{
				case "delete":
					
				break;
				case "rename":
				break;	
			}
		}
		that.changeDir(currentDir, function(aEntries){
			document.dispatchEvent(new CustomEvent('fsChange', { 'detail': {data: aEntries, dir:currentDir} }));
		});
	});
	
	this.getStoragesList = function(fSuccessCb, fErrCb){
		//default callback for error handling
		fErrCb = fErrCb || function(oErr){
			console.log(oErr);
		};
		tizen.filesystem.listStorages(fSuccessCb, fErrCb);
	}
	
	this.getCurrentDir = function(){
		return currentDir
	}
	
	this.setRoot = function(sRoot){
		rootDir = sRoot;
		return this
	}
	
	this.changeDir = function(sPath, fSuccess, fError){
		//checking if there's a directory under that path
		tizen.filesystem.resolve(sPath, function(oDir){
			//directory successfully retrieved

			//extending successful callback in order to add some default behaviour
			var fSuccessExt = function(aFiles){
				//you cant go upper if you're in root
				var sParentPath = "";
				if(oDir.parent && !oDir.parent.readOnly)
				{
					sParentPath = oDir.parent.fullPath;
				}
				else if(rootDir !== sPath)
				{
					sParentPath = rootDir;
				}
				aFiles = sParentPath ? [{isFile:false, isDirectory:true, name: "..", created:"Родительская директория", fullPath: sParentPath}].concat(aFiles) : aFiles;
				fSuccess(aFiles);
			};

			//default error handler (can't be undefined in filesystem.resolve method)
			fError = fError || function(oErr){
				console.log(oErr);
			};

			tizen.filesystem.resolve(sPath, function(oResDir){
				currentDir = sPath;
				oResDir.listFiles(fSuccessExt, fError);
			}, fError, "rw");
		}, function(oErr){
			//directory wasnt found
			console.log(oErr);
		});
	}
	
	//returns this for chaining
	this.setCurrentDir = function(oDir){
		currentDir = oDir.isDirectory ? oDir : currentDir;
		return this
	}
	
	//returns contents of required directory
	this.getContents = function(sDirPath, fSuccessCb, fErrCb){
		//gotta check if directory is valid
		if(sDirPath)
		{
			tizen.filesystem.resolve(sDirPath, function(oResDir){
				oResDir.listFiles(fSuccessCb, fErrCb);
			}, $.noop, "rw");
		}
	}
	
	//adds new file or directory to the current directory
	this.addNew = function(bFile, sFileName, fSuccess, fError){
		fSuccess = fSuccess || $.noop;
		fError = fError || $.noop;
		tizen.filesystem.resolve(currentDir, function(oResDir){
			try
			{
				if(bFile)
					fSuccess(oResDir.createFile(sFileName));
				else
					fSuccess(oResDir.createDirectory(sFileName));
				that.changeDir(currentDir, function(aEntries){
					document.dispatchEvent(new CustomEvent('fsChange', { 'detail': {data: aEntries, dir:currentDir} }));
				});
			}
			catch(oErr)
			{
				fError(oErr);
			}
		}, fError, "rw");
	}
}