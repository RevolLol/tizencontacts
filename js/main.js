( function () {
    window.addEventListener( 'tizenhwkey', function( ev ) {
        if( ev.keyName === "back" ) {
            var activePopup = document.querySelector( '.ui-popup-active' ),
                page = document.getElementsByClassName( 'ui-page-active' )[0],
                pageid = page ? page.id : "";

            if( pageid === "main" && !activePopup ) {
                try {
                    tizen.application.getCurrentApplication().exit();
                } catch (ignore) {
                }
            } else {
                window.history.back();
            }
        }
    } );
    
    this.fsManager = new fsManager();
    this.storagesDOM = new storagesDOM();
    this.filesDOM = new filesDOM();
    this.filesDOM.getDomRef().appendTo($("#mainContent"));
    this.drawer = new baseDOM($("#leftdrawer"), null, null, "Drawer");
    this.popUpManager = new popUpManager();
    var that = this;
    
    //button for opening a drawer
    $("#hambBtn").on("click", function(){
    	try {
    		that.drawer.getTizenObj().open();
		} catch (e) {
		}
    });
    
    //changing current path displayed in header, managing floating actions
    document.addEventListener("fsChange", function(oEvent){
    	$("#currPath").text(oEvent.detail.dir || "");
    	if(!that.flActions)
    	{
    		that.flActions = new floatingActions([
                {
                	on:{
                		click:function(){
                			that.popUpManager.showAddDialog(true, function(sName){
                				that.fsManager.addNew(true, sName);
                			});
                		}
                	},
                	"data-icon": "file"
                },
                {
                	on:{
                		click:function(){
                			that.popUpManager.showAddDialog(false, function(sName){
                				that.fsManager.addNew(false, sName);
                			});
                		}
                	},
                	"data-icon": "folder"
                }
    		]);
    		that.flActions.getDomRef().appendTo($("#main"));
    	}
    });
    
    //swiping from left to right opens drawer
    //reversed swipe is also implemented and should be closing the drawer, but for some reason is doesnt happen
	$("#main")[0].addEventListener("pagebeforeshow", function() {
		var oContent = $("#main")[0];
		tau.event.enableGesture(oContent, new tau.event.gesture.Swipe( {
		   orientation: "horizontal"
		}));

		oContent.addEventListener("swipe", function(oEvent) {
			if(oEvent.detail.direction === "right")
			{
				that.drawer.getTizenObj().open();
			}
			else if(oEvent.detail.direction === "right") {
				that.drawer.getTizenObj().close();
			}
		});
	});
    
    this.fsManager.getStoragesList(function(aSt){
    	aSt.forEach(function(oSt){
    		oSt.on = {
        			click: function(){
        				that.drawer.getTizenObj().close();
        			}
        		};
        	}
    	);
    	that.storagesDOM.addData(aSt).getDomRef().appendTo($("#leftdrawer"));
    	var nIntStorage = 0;
    	for(nIntStorage; nIntStorage < aSt.length && !~aSt[nIntStorage].label.search(/.*internal.*/gi); nIntStorage++);
    	if(nIntStorage !== aSt.length)
		{
    		
    		//found internal storage
    		that.fsManager.setRoot(aSt[nIntStorage].label);
    		document.dispatchEvent(new CustomEvent('dirChange', { 'detail': {dir: aSt[nIntStorage].label} }));
		}
    });
	
} () );