/**
 * base class for prototyping main dom objects
 * @param {object|jQuery|string} vDomRef DOM element or it's name that is gonna represent JS object
 * @param {any[]|object} vData information to be passed to the element for creating internal structure
 * @param {object} oProperties DOM properties of the element
*/
function baseDOM(vDomRef, vData, oProperties, sTizenClass){
	this._data = vData || null;
	this._tizenClass = sTizenClass || "";
	oProperties = oProperties || {};
	if(vDomRef instanceof jQuery)
		this._domRef = vDomRef;
	else
		this.createDomRef(vDomRef, oProperties, true);
	//binding all public functions to object in order not to lose context
	for (var vProp in this) 
	{
		if($.isFunction(this[vProp]))
		{
			this[vProp] = this[vProp].bind(this);
		}
	}
}

baseDOM.prototype = {
	/**
	 * @returns {jQuery}
	*/
	getDomRef: function(){
		return this._domRef
	},
	
	/**
	 * creates and returns tizen object based on it's class name and DOM reference (if one is valid)
	 * @returns {tau.widget}
	*/
	getTizenObj: function(){
		if(tau.widget && tau.widget[this._tizenClass])
			return tau.widget[this._tizenClass](this._domRef[0]);
		return null
	},
	
	/**
	 * basic method for creating dom objects
	 * @todo allow customizing long press duration
	 * @param {string|object} vElemName 
	 * @param {object} oElemProps
	 * @param {boolean} bRoot If true, assigns element to this._domRef, otherwise creates an independent node
	 * @returns {jQuery|this}
	*/
	createDomRef: function(vElemName, oElemProps, bRoot){
		if(!vElemName)
			return null
		//in case argument wasnt passed we convert if from undefined to false
		bRoot = !!bRoot;
		var oNewElement = $(vElemName, oElemProps), nLPressStart = 0, nLPressTimer, nLPressDuration = 1000;
		//implementing long clicks for each DOM element, that is going to be created with that method
		oNewElement.on("touchend", function(oEvent) {
			oEvent.stopPropagation();
			oEvent.preventDefault();
		    if(new Date().getTime() < ( nLPressStart + nLPressDuration ))
		    {
		      clearTimeout(nLPressTimer);
		      //touch events dont trigger click automatically
		      if(!oEvent.target._bWasMoved)
		    	  this.dispatchEvent(new Event('click'));
		      //$(this).trigger("click");
		    }
		    else
	    	{
			     try
			     {
			        //getting original click handler
			     	var fClick = $._data(this, "events").click[0].handler;
			        //replacing it with our own click handler
			        $(this).off("click").click(function(){
			        		console.log("intercepted click");
			        		$(this).off("click").click(fClick);
			        });
				 }
			     catch(oErr)
			     {
			    	 console.log(oErr);
			     }
	    	}
		    oEvent.target._bWasMoved = false;
	    }).on("touchstart", function(oEvent) {
				oEvent.stopPropagation();
				nLPressStart = new Date().getTime();
			    nLPressTimer = setTimeout(function(){
					//long press happened
				    clearTimeout(nLPressTimer);
				    
			        //$(oEvent.target).trigger("longclick");
			        oEvent.target.dispatchEvent(new CustomEvent('longclick', {bubbles:true, cancelable: true}));
			    }, nLPressDuration);
		}).on("touchmove", function(oEvent) {
			//preventing click and longclick from firing if user scrolls
			oEvent.target._bWasMoved = true;
			if(nLPressTimer)
				clearTimeout(nLPressTimer);
		});
		
		
		if(bRoot)
		{
			this._domRef = oNewElement;
			return this
		}
		return oNewElement
	},
	
	destroyDomRef:function(){
		if(this._domRef && this._domRef instanceof jQuery)
			this._domRef.remove();
	},
	
	createElem: function(fInit){
		if(this._domRef && this._data && fInit)
		{
			var oDOMRefParent  = this._domRef.parent();
			fInit.call(this);
			//detecting if there was a parent before
			if(oDOMRefParent.length)
			{
				oDOMRefParent.append(this._domRef);
			}
		}
	},
	
	addData: function(vData){
		if(vData)
			this._data = vData;
		if(this._domRef)
		{
			this._domRef.empty();
		}
		return this
	},
	
	subToEvent:function(sEventName, oEmitElem, fHandler){
		if(oEmitElem.addEventListener && fHandler)
		{
			fHandler = fHandler.bind(this);
			oEmitElem.addEventListener(sEventName, fHandler);
		}
	}
}

/**
 * class for managing list of storages
 * @param {object|jQuery|string} vDomRef DOM element or it's name that is gonna represent JS object
 * @param {any[]|object} vData information to be passed to the element
*/
function storagesDOM(aData, oProperties)
{
	//applying baseDOM constructor to our extended element
	baseDOM.call(this, "<ul>", aData, oProperties || {"class":"ui-listview storagesList"});
	this._tizenClass = "Listview";
	this.createElem();
}
//setting baseDOM as prototype and adding specific methods for storagesDOM
storagesDOM.prototype = $.extend(Object.create(baseDOM.prototype), {
	
	addData:function(aData){
		baseDOM.prototype.addData.apply(this, arguments);
		this.createElem();
		return this
	},
	
	createElem: function(){
		baseDOM.prototype.createElem.call(this, function(){
			var that = this;
			$().append.apply(this._domRef, this._data.map(function(oStData, nIndex){
				var oLi = that.createDomRef("<li>", 
					{
						text:oStData["label"] || "storage" + nIndex,
						"class": oStData["class"] || "ui-drawer-main-list",
						on: {
							click: function(oEvent){
								document.dispatchEvent(new CustomEvent('dirChange', { 'detail': {dir: $(oEvent.target).text()} }));
								try
								{

									console.log("click");
									oStData.on.click();
								}
								catch (oErr) {
									
								}
							},
							longclick: function(oEvent){
								console.log($(oEvent.target).text() || "");
								document.dispatchEvent(new CustomEvent('showActions', { 'detail': {target: {name:"PEANUTBUTTER"}} }));
							}
						}
					}, 
					false
				);
				return oLi
				})	
			);
		});
	}
});

/**
 * class for managing list of files
*/
function filesDOM(aData, oProperties)
{
	//applying baseDOM constructor to our extended element
	baseDOM.call(this, "<ul>", aData, oProperties || {"class":"ui-listview filesList"});
	this._tizenClass = "Listview";
	this.createElem();
	//making a global event subscription for content data refreshing
	this.subToEvent("fsChange", document, function(oEvent){
		var oEventData = oEvent.detail;
		this.addData(oEventData.data);
	});
}
//setting baseDOM as prototype and adding specific methods for filesDOM
filesDOM.prototype = $.extend(Object.create(baseDOM.prototype), {
	
	addData:function(aData){
		baseDOM.prototype.addData.apply(this, arguments);
		this.createElem();
		return this
	},
	createElem:function(){
		baseDOM.prototype.createElem.call(this, function(){
			var that = this;
			$().append.apply(this._domRef, this._data.map(function(oFile, nIndex){
				var oNewFile = that.createDomRef("<li>", 
					{
						"class":"ui-li-static",
						"on": {
							click: function(oEvent){
								if(!oFile.isFile)
									document.dispatchEvent(new CustomEvent('dirChange', { 'detail': {dir: oFile.fullPath} }));
							},
							longclick:function(oEvent){
								if(oFile.name !== "..")
									document.dispatchEvent(new CustomEvent('showActions', { 'detail': {target: oFile} }));
							}
						}
					},
					false
				);
				var oInfoDiv = $('<div class="contInfo">');
				oInfoDiv.append(
					$('<span>',{
						"class":"fileName",
						text:oFile.name || oFile.path
					}),
					$('<span>',{
						"class":"fileInfo",
						text: oFile.created.toLocaleString ? oFile.created.toLocaleString() : oFile.created.toString()
					})
				);
				oNewFile.append(
					$("<img>", {
						"class": "icon fileIcon",
						"src": oFile.isDirectory ? "./css/res/folder.png" : "./css/res/file.png"
					}),
					oInfoDiv
				);
				return oNewFile
			}));
		}); 
	}
});


function popUpManager(vData, oProperties)
{
	//element, that called a pop-up
	this._target = null; 
	this._actionsDialog = {};
	this._init();
}

$.extend(popUpManager.prototype,{
	_init: function(){
		
		//handles different action options
		var fHelper = (function(sActionName){
			sActionName = (typeof sActionName === "string") ?  sActionName.toLowerCase() : "";
			this._actionsDialog.getTizenObj().close();
			if(this._target)
			{
				var sName = this._target.name || this._target.path;
				switch(sActionName)
				{	
					case "delete":
						this.showPopUp({
								header: "Confirm delete",
								content: "Are you sure you want to delete " + sName + "?",
								footer: [{text:"Yes"}, {text:"Cancel"}]
							});
					break;
					case "rename":
						this.showPopUp({
								header: "Enter new filename",
								content: $("<input>", {"data-clear-btn": true, value: sName, type:"text", on:{click:function(){this.focus()}}}),
								footer: [{text:"Ok"}, {text:"Cancel"}]
							});
					break;
					case "copy":
						this.showToast({content: sName + " successfully copied"});
					break;
					case "cut":
						this.showToast({content: sName + " successfully cut"});
					break;
					default:
					break;
				}
			}
		}).bind(this);
		
		this._actionsDialog = new popUp({
			header: {text:"File actions"},
			content: [
	          {text:"Delete", on:{
	        	  click: function(){
	        		  fHelper("delete");
	        	  }
	          }}, 
	          {text:"Rename", on:{
	        	  click: function(){
	        		  fHelper("Rename");
	        	  }
	          }}, 
	          {text:"Copy", on:{
	        	  click: function(){
	        		  fHelper("Copy");
	        	  }
	          }}, 
	          {text:"Cut", on:{
	        	  click: function(){
	        		  fHelper("Cut");
	        	  }
	          }}
			],
			
			footer: {text:"Close", on:{click:(function(){
				this._target = null;
			}).bind(this)}}
		});
		

		baseDOM.prototype.subToEvent.call(this, "showActions", document, function(oEvent){
			if(oEvent.detail.target)
			{
				console.log(oEvent.detail.target);
				this.showActions(oEvent.detail.target);	
			}
		});
	},
	
	//target is mandatory
	showActions:function(oTarget){
		this._target = oTarget; 
		var oTizenPopup = this._actionsDialog.getTizenObj();
		if(oTizenPopup && this._target)
		{
			oTizenPopup.open();
		}
	},
	
	_tempPopUp: function(oData, oTarget, oProps){
		if(oTarget)
			this._target = oTarget;
		var oTempPopUp = new popUp(oData, oProps || {});
		oTempPopUp.getDomRef().on("popupafterclose", (function() 
		   {
			//removing temp popups and overlays from DOM since TAU doesnt handle it
				oTempPopUp.getDomRef().prev(".ui-popup-overlay").remove();
				oTempPopUp.destroyDomRef();
				this._target = null;
		   }).bind(this)
		);
		oTempPopUp.getTizenObj().open();
	},
	
	//target is optional here
	showPopUp: function(oData, oTarget){
		this._tempPopUp(oData, oTarget);
	},
	
	showAddDialog:function(bFile, fSuccess){
		var sName = bFile ? "file" : "folder ";
		var oInput = $("<input>", {"data-clear-btn": true, value: sName, type:"text", on:{click:function(){this.focus()}}});
		this.showPopUp({
			header: "Enter new " + sName + "name",
			content: oInput,
			footer: [{text:"Ok", on:{click: function(){
				fSuccess(oInput.val());
			}}}, {text:"Cancel"}]
		});
	},
	
	showToast: function(oData){
		this._tempPopUp(oData, null, {
			"class": "ui-popup ui-popup-toast"
		});
	}
});

function popUp(oData, oProperties)
{
	//applying baseDOM constructor to our extended element
	baseDOM.call(this, "<div>", oData, oProperties || {"class":"ui-popup"});
	this._tizenClass = "Popup";
	this.createElem();
}

popUp.prototype = $.extend(Object.create(baseDOM.prototype), {
	addData:function(oData){
		baseDOM.prototype.addData.apply(this, arguments);
		this.createElem();
		return this
	},
	
	_createHeader:function(){
		if(this._data.header)
		{
			return this.createDomRef("<div>", 
					{
						"class":"ui-popup-header",
						"on": this._data.header["on"] || {},
						"text": (typeof this._data.header === "string") ? this._data.header : this._data.header["text"] || "Information"
					},
				false
			);
		}
		return null
	},
	
	_createContent:function(){
		if(this._data.content)
		{
			var oContent = this.createDomRef("<div>", 
					{
						"class":"ui-popup-content popup-content-padding popUpContent"
					},
					false
			);
			switch(typeof this._data.content){
			//in case we're constructing simple information dialog like an alert
			case "string":
				oContent.text(this._data.content);
			break;
			
			//in case we're constructing an action dialog
			case "object":
				//action dialog  with a list of options
				if(Array.isArray(this._data.content))
				{
					var that = this;
					//appending ul list and it's contents (li elements) to pop up content div
					oContent.append($().append.apply(this.createDomRef("<ul>", {"class":"ui-listview actionsList"}), this._data.content.map(function(oListElem){
						return that.createDomRef("<li>", 
							{
								"class":"ui-li-static",
								"text":oListElem.text,
								"on": oListElem.on || {
									click: function(oEvent){
										console.log("clicked option");
									},
									longpress:function(oEvent){
										console.log("long clicked option");
									}
								}
							},
							false
						);
					}),false));
				}
				//action dialog with the only option
				else
				{
					//a fallback for putting custom stuff into content
					if(this._data.content instanceof jQuery)
						oContent.append(this._data.content);
					else
						oContent.append(this.createDomRef("<div>",{
							"class": this._data.content["class"] || "",
							"text":this._data.content.text,
							"on": this._data.content.on
						}, false));
				}
			break;
			}
			return oContent
		}
		return null
	},
	
	_createFooter:function(){
		var that = this,
			oFooter = this.createDomRef("<div>", 
				{
					"class":"ui-popup-footer"
				},
				false
			);
		if(this._data.footer && !$.isEmptyObject(this._data.footer))
		{
			this._data.footer = Array.isArray(this._data.footer) ? this._data.footer : [this._data.footer];
			oFooter.append.apply(oFooter, this._data.footer.map(function(oElem, nInd, aButtons){
				return that.createDomRef("<button>",
						{
							"class":"ui-btn" + (oElem["class"] || ""),
							"data-inline":true,
							//buttons stretch to full width of footer by default, so we calculate and set their width manually
							"style":"width:" + 100/aButtons.length + "%",
							"text":oElem.text,
							"on":{
								click: function(oEvent){
									that.getTizenObj().close();
									if(oElem.on && oElem.on.click)
									{
										oElem.on.click();
									}
								}
							}
						}
					);
				})
			);
		}
		return oFooter
	},
	
	createElem: function(){
		baseDOM.prototype.createElem.call(this, function(){
			$().append.call(this._domRef, this._createHeader(), this._createContent(), this._createFooter());
		});
	}
});

function floatingActions(oData, oProperties)
{
	//applying baseDOM constructor to our extended element
	baseDOM.call(this, "<div>", oData, oProperties || {"class":"ui-floatingactions"});
	this._tizenClass = "FloatingActions";
	this.createElem();
}

//setting baseDOM as prototype and adding specific methods for filesDOM
floatingActions.prototype = $.extend(Object.create(baseDOM.prototype), {
	
	addData:function(aData){
		baseDOM.prototype.addData.apply(this, arguments);
		this.createElem();
		return this
	},
	createElem:function(){
		baseDOM.prototype.createElem.call(this, function(){
			var that = this;
			this._data = Array.isArray(this._data) ? this._data : [this._data];
			$().append.apply(this._domRef, this._data.map(function(oButton){
				return that.createDomRef("<button>", 
					{
						"class":"ui-floatingactions-item" + " ui-icon-" + oButton["data-icon"],
						"on": oButton.on,
						"text": oButton.text || ""
					},
					false
				);
			}));
		}); 
	}
});
